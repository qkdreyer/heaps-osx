### Usage

```sh
cp -R /path/to/artifacts/* dist
PRODUCT_NAME=TCV1 PRODUCT_BUNDLE_IDENTIFIER=dev.qkdreyer.creamy-viking-1 SIGNING_IDENTITY=5022DC81A7F9857057BBF85E0B0B4ADAF3C2DEDB make init
BIN=creamy-viking-1 make
BIN=creamy-viking-1 API_KEY=MU6YKT8PHN make notarize
SESSION=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx BIN=creamy-viking-1 API_KEY=MU6YKT8PHN make notarize
```
