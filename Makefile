ifndef PRODUCT_NAME
	PRODUCT_NAME := $$(cat appdmg.json | jq -r '.title')
endif
ifndef SIGNING_IDENTITY
	SIGNING_IDENTITY := $$(cat appdmg.json | jq -r '."code-sign"."signing-identity"')
endif
ifndef PRODUCT_BUNDLE_IDENTIFIER
	PRODUCT_BUNDLE_IDENTIFIER := $$(cat appdmg.json | jq -r '."code-sign".identifier')
endif

.phony: all

all:
	chmod u+x dist/$(BIN)
	platypus -a $(PRODUCT_NAME) -I $(PRODUCT_BUNDLE_IDENTIFIER) -y -o 'None' -i icon.icns -f dist/hlboot.dat -f dist/libSDL2-2.0.0.dylib -f dist/libhl.dylib -f dist/libmbedtls.10.dylib -f dist/libopenal.1.dylib -f dist/libpng16.16.dylib -f dist/libuv.1.dylib -f dist/libvorbis.0.dylib -f dist/libvorbisfile.3.dylib -f dist/$(BIN) -C $(BIN) main.sh dist/$(PRODUCT_NAME)
	npx appdmg appdmg.json dist/$(PRODUCT_NAME).dmg
	chmod u+x dist/$(PRODUCT_NAME).dmg

init:
	which -s platypus || brew install platypus
	which -s gsed || brew install gnu-sed
	cp appdmg.json.dist appdmg.json
	gsed -i "s/PRODUCT_NAME/$(PRODUCT_NAME)/" appdmg.json
	gsed -i "s/SIGNING_IDENTITY/$(SIGNING_IDENTITY)/" appdmg.json
	gsed -i "s/PRODUCT_BUNDLE_IDENTIFIER/$(PRODUCT_BUNDLE_IDENTIFIER)/" appdmg.json

notarize:
ifndef SESSION
	chmod -R a+xr dist/$(PRODUCT_NAME).app
	find dist/$(PRODUCT_NAME).app -type f -exec codesign --deep --force --verify --verbose --timestamp --options=runtime --entitlements "App.entitlements" --sign $(SIGNING_IDENTITY) {} +
	codesign --deep --force --verify --verbose --timestamp --options=runtime --entitlements "App.entitlements" --sign $(SIGNING_IDENTITY) dist/$(PRODUCT_NAME).app
	ditto -c -k --sequesterRsrc --keepParent dist/$(PRODUCT_NAME).app dist/$(PRODUCT_NAME).archive.zip
	xcrun altool --notarize-app -f dist/$(PRODUCT_NAME).archive.zip --primary-bundle-id $(PRODUCT_BUNDLE_IDENTIFIER) --apiKey $(API_KEY) --apiIssuer $(ISSUER_ID)
else
	xcrun altool --notarization-info $(SESSION) --apiKey $(API_KEY) --apiIssuer $(ISSUER_ID) && xcrun stapler staple dist/$(PRODUCT_NAME).app && zip -r dist/$(PRODUCT_NAME).zip dist/$(PRODUCT_NAME).app
endif

clean:
	rm -rf dist/* appdmg.json
